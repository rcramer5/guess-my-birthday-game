from random import randint
name = input("Hi! What is your name?")

guess = int()
monthNum = randint(1,12)
yearNum = randint(1924,2004)
maxYear=int(2004)
minYear=int(1924)
minMonth=int(1)
maxMonth=int(12)

#while guess < 5:                                                                                                                               // previous while loop (requirements for class are to use a for loop with range)        
for guess in range(1,6):
    #guess = guess + 1                                                                                                                          // from a previous build using a while loop
    if guess < 6:
        print("Guess", guess, ":", name, "were you",
        "born on" , monthNum, "/", yearNum,"?", "(yes, earlier, later)")
        answer = input()
        
        if answer == "yes":
            print("I knew it!")
            break
        elif answer == "earlier":
            print("Drat! Lemme try again...")
            maxYear = yearNum
            if minYear > 1924 :
                yearNum = randint(minYear,maxYear)
                monthNum = randint(1,12)
                
            elif minYear == 1924:
                yearNum = randint(1924, maxYear)
                monthNum = randint(1,12)

            elif minYear == maxYear:   # attempt at focusing the month once the year is decided, however the years will never be equal as i dont have the minyear and maxYear specific enough, due to the months being a more specific variable. (cant use >= or <=)
                yearNum = minYear
                monthNum = randint(1,12)
                monthAnswer = input("Now that I have the year correct, is the month earlier or later?")
                if monthAnswer == "earlier":
                    maxMonth = monthNum
                    monthNum = randint(minMonth,maxMonth)
                elif monthAnswer == "later":
                    minMonth = monthNum
                    monthNum = randint(minMonth, maxMonth)
                else:
                    print("you lied to me :(")
                    

                    
        elif answer == "later":
            #print("Drat! Lemme try again...")
            minYear = yearNum
            if maxYear < 2004:
                yearNum = randint(minYear,maxYear)
                monthNum = randint(1,12)
                
            elif maxYear == 2004:
                yearNum = randint(minYear,2004)
                monthNum = randint(1,12)
            
            elif minYear == maxYear:
                yearNum = minYear
                monthNum = randint(1,12)
                monthAnswer = input("Now that I have the year correct, is the month earlier or later?")
                if monthAnswer == "earlier":
                    maxMonth = monthNum
                    monthNum = randint(minMonth,maxMonth)
                elif monthAnswer == "later":
                    minMonth = monthNum
                    monthNum = randint(minMonth, maxMonth)
                else:
                    print("you lied to me :(")
            
            else:
                #guess = guess - 1                                                                      // from a previous build using a while loop
                print("Try another response:")
else:
    print("I dont have time for this...")